 program zComposite;

{$mode objfpc}{$H+}

uses
  {$I SynDprUses.inc}  // use FastMM4 on older Delphi, or set FPC threads
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp,
  { you can add units after this }
  Crt,
  SynCommons,          // framework core
  SynLog,
  mORMot,              // RESTful server & ORM
  mORMotHttpClient,    // HTTP client to a mORMot RESTful server
  mORMotHttpServer,
  uBusinessInterface,
  uCompositeInterface,
  uSDContactHandler;

type

  { TApplicationServer }

  TApplicationServer = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

var
  aModelClient: TSQLModel;

  aModel: TSQLModel;
  aServer: TSQLRestServerFullMemory;
  aHTTPServer: TSQLHttpServer;

{ TApplicationServer }

procedure TApplicationServer.DoRun;
var
  ErrorMsg: String;
  //I: IESInBound;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('h', 'help');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }
  with TSQLLog.Family do begin
    Level := LOG_VERBOSE;
    EchoToConsole := LOG_VERBOSE;
  end;

  aModelClient := TSQLModel.Create([], ROOT_NAME_BUSINESS);
  try
    aBusinessClient := TSQLHttpClient.Create('localhost', PORT_NAME_BUSINESS, aModelClient);

    aBusinessClient.ServiceDefine([IESInBound],sicShared);
    //aBusinessClient.ServerTimeStampSynchronize;
    //if aBusinessClient.Services['ESInBound'].Get(I) then I.ForTest('log 5');
    try
      {writeln('Business server is running.'#10);
      write(#10'Press Any Key to quit');

      repeat

      until KeyPressed;
      //readln;
      writeln('Composite Server To Be Shutdown.'#10);}
      // create a Data Model
      aModel := TSQLModel.Create([],ROOT_NAME_COMPOSITE);
      try
        // initialize a TObjectList-based database engine
        //aServer := TSQLRestServerFullMemory.Create(aModel,'test.json',false,false);
        aServer:= TSQLRestServerFullMemory.Create(aModel, false);
        try
          // register our interface service on the server side
          aServer.ServiceDefine(TSDContactHandler,[ISDContactHandler],sicShared);
          // launch the HTTP server
          aHTTPServer := TSQLHttpServer.Create(PORT_NAME_COMPOSITE,[aServer],'+' {$ifndef ONLYUSEHTTPSOCKET},useHttpApiRegisteringURI{$endif});
          try
            aHTTPServer.AccessControlAllowOrigin := '*'; // for AJAX requests to work

            writeln('Business server is running.'#10);
            write(#10'Press Any Key to quit');

            repeat

              //parent.ProcessMessage;
            until KeyPressed;
            //readln;
            writeln('Business Server To Be Shutdown.'#10);


          finally
            aHTTPServer.Free;
          end;
        finally
          aServer.Free;
        end;
      finally
        aModel.Free;
      end;

    finally
      aBusinessClient.Free;
    end;
  finally
    aModel.Free;
  end;

  // stop program loop
  Terminate;
end;

constructor TApplicationServer.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TApplicationServer.Destroy;
begin
  inherited Destroy;
end;

procedure TApplicationServer.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' -h');
end;

var
  Application: TApplicationServer;
begin
  Application:=TApplicationServer.Create(nil);
  Application.Title:='zComposite Application Server';
  Application.Run;
  Application.Free;
end.

