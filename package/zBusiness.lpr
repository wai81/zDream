program zBusiness;

{$mode objfpc}{$H+}

uses
  {$I SynDprUses.inc}  // use FastMM4 on older Delphi, or set FPC threads
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp,
  { you can add units after this }
  Crt,
  SynCommons,          // framework core
  SynLog,
  mORMot,              // RESTful server & ORM
  mORMotHttpClient,    // HTTP client to a mORMot RESTful server
  mORMotHttpServer,
  uModel,              // data model unit, shared between server and client
  uParty,
  uBusinessInterface,
  uESInBound;

type

  { TZBusiness }

  TZBusiness = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    //DataCenterModel: TSQLModel;
    //DataCenterHTTPClient: TSQLHttpClient;

    PartyModel: TSQLModel;
    PartyHTTPClient: TSQLHttpClient;

    ProductModel: TSQLModel;
    ProductHTTPClient: TSQLHttpClient;

    OrderModel: TSQLModel;
    OrderHTTPClient: TSQLHttpClient;

    AccountingModel: TSQLModel;
    AccountingHTTPClient: TSQLHttpClient;

    WorkEffortModel: TSQLModel;
    WorkEffortHTTPClient: TSQLHttpClient;

    ShipmentModel: TSQLModel;
    ShipmentHTTPClient: TSQLHttpClient;

    MarketingModel: TSQLModel;
    MarketingHTTPClient: TSQLHttpClient;

    ManufacturingModel: TSQLModel;
    ManufacturingHTTPClient: TSQLHttpClient;

    HumanresModel: TSQLModel;
    HumanresHTTPClient: TSQLHttpClient;

    CommonModel: TSQLModel;
    CommonHTTPClient: TSQLHttpClient;

    ContentModel: TSQLModel;
    ContentHTTPClient: TSQLHttpClient;

    SecurityModel: TSQLModel;
    SecurityHTTPClient: TSQLHttpClient;

    ServiceModel: TSQLModel;
    ServiceHTTPClient: TSQLHttpClient;

    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

var
  aModel: TSQLModel;
  aServer: TSQLRestServerFullMemory;
  aHTTPServer: TSQLHttpServer;

{ TZBusiness }

procedure TZBusiness.DoRun;
var
  ErrorMsg: String;
  fsClient: TESInBound;
  aPerson: TSQLPartyType;
  aID: integer;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('h', 'help');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }
  with TSQLLog.Family do begin
    Level := LOG_VERBOSE;
    EchoToConsole := LOG_VERBOSE;
  end;

  //DataCenterModel := CreateDataCenterModel;
  PartyModel := CreatePartyModel;
  ProductModel := CreateProductModel;
  OrderModel := CreateOrderModel;
  AccountingModel := CreateAccountingModel;
  WorkEffortModel := CreateWorkEffortModel;
  ShipmentModel := CreateShipmentModel;
  MarketingModel := CreateMarketingModel;
  ManufacturingModel := CreateManufacturingModel;
  HumanresModel := CreateHumanresModel;
  CommonModel := CreateCommonModel;
  ContentModel := CreateContentModel;
  SecurityModel := CreateSecurityModel;
  ServiceModel := CreateServiceModel;
  try
    //DataCenterHTTPClient := TSQLHttpClient.Create('localhost','3618', DataCenterModel, False);
    PartyHTTPClient := TSQLHttpClient.Create('localhost','3618', PartyModel, False);
    ProductHTTPClient := TSQLHttpClient.Create('localhost','3618', ProductModel, False);
    OrderHTTPClient := TSQLHttpClient.Create('localhost','3618', OrderModel, False);
    AccountingHTTPClient := TSQLHttpClient.Create('localhost','3618', AccountingModel, False);
    WorkEffortHTTPClient := TSQLHttpClient.Create('localhost','3618', WorkEffortModel, False);
    ShipmentHTTPClient := TSQLHttpClient.Create('localhost','3618', ShipmentModel, False);
    MarketingHTTPClient := TSQLHttpClient.Create('localhost','3618', MarketingModel, False);
    ManufacturingHTTPClient := TSQLHttpClient.Create('localhost','3618', ManufacturingModel, False);
    HumanresHTTPClient := TSQLHttpClient.Create('localhost','3618', HumanresModel, False);
    CommonHTTPClient := TSQLHttpClient.Create('localhost','3618', CommonModel, False);
    ContentHTTPClient := TSQLHttpClient.Create('localhost','3618', ContentModel, False);
    SecurityHTTPClient := TSQLHttpClient.Create('localhost','3618', SecurityModel, False);
    ServiceHTTPClient := TSQLHttpClient.Create('localhost','3618', ServiceModel, False);
    //DataCenterHTTPClient.Model.add;
    //aClient := TSQLHttpClientWinHTTP.Create('localhost','3618', PartyModel);
    //TSQLHttpClient(aClient).SetUser('User','synopse');

    try
      // create a Data Model
      aModel := TSQLModel.Create([],ROOT_NAME_BUSINESS);
      try
        // initialize a TObjectList-based database engine
        //aServer := TSQLRestServerFullMemory.Create(aModel,'ForTest.json',false,false);
        aServer:= TSQLRestServerFullMemory.Create(aModel, false);
        try
          // register our interface service on the server side
          aServer.ServiceDefine(TESInBound,[IESInBound],sicShared);
          // launch the HTTP server http://localhost:2020/business/SInBound.ForTest
          aHTTPServer := TSQLHttpServer.Create(PORT_NAME_BUSINESS,[aServer],'+' {$ifndef ONLYUSEHTTPSOCKET},useHttpApiRegisteringURI{$endif});
          try
            aHTTPServer.AccessControlAllowOrigin := '*'; // for AJAX requests to work

            fsClient:= TESInBound.Create;

            writeln('Business server is running.'#10);
            write(#10'Press Any Key to quit');

            {repeat
              fsClient.Run;

              //parent.ProcessMessage;
            until KeyPressed;}
            readln;
            writeln('Business Server To Be Shutdown.'#10);


          finally
            aHTTPServer.Free;
          end;
        finally
          aServer.Free;
        end;
      finally
        aModel.Free;
      end;

    finally
      //DataCenterHTTPClient.Free;
      PartyHTTPClient.Free;
      ProductHTTPClient.Free;
      OrderHTTPClient.Free;
      AccountingHTTPClient.Free;
      WorkEffortHTTPClient.Free;
      ShipmentHTTPClient.Free;
      MarketingHTTPClient.Free;
      ManufacturingHTTPClient.Free;
      HumanresHTTPClient.Free;
      CommonHTTPClient.Free;
      ContentHTTPClient.Free;
      SecurityHTTPClient.Free;
      ServiceHTTPClient.Free;
    end;
  finally
    //DataCenterModel.Free;
    PartyModel.Free;
    ProductModel.Free;
    OrderModel.Free;
    AccountingModel.Free;
    WorkEffortModel.Free;
    ShipmentModel.Free;
    MarketingModel.Free;
    ManufacturingModel.Free;
    HumanresModel.Free;
    CommonModel.Free;
    ContentModel.Free;
    SecurityModel.Free;
    ServiceModel.Free;
  end;

  // stop program loop
  Terminate;
end;

constructor TZBusiness.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TZBusiness.Destroy;
begin
  inherited Destroy;
end;

procedure TZBusiness.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' -h');
end;

var
  Application: TZBusiness;
begin
  Application:=TZBusiness.Create(nil);
  Application.Title:='zBusiness Application Server';
  Application.Run;
  Application.Free;
end.

