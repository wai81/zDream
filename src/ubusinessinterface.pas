unit uBusinessInterface;

{$mode ObjFPC}{$H+}

interface

uses
  SynCommons,
  mORMot;

type
  // http://localhost:2021/business/ESInBound.ForTest
  IESInBound = interface(IInvokable)
    ['{E296E14D-CA65-4BCB-BF1D-D6B650F96434}']
    procedure ForTest(aMessage: string);
  end;

const
  ROOT_NAME_BUSINESS = 'business';
  PORT_NAME_BUSINESS = '2020';
  APPLICATION_NAME_BUSINESS = 'zBusiness';

var
  aBusinessClient: TSQLRestClientURI;

implementation

initialization
  // so that we could use directly IESInBound instead of TypeInfo(IESInBound)
  TInterfaceFactory.RegisterInterfaces([TypeInfo(IESInBound)]);

end.

