unit uSDContactHandler;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  SynCommons,
  mORMot,
  uBusinessInterface,
  uCompositeInterface;

type
  TSDContactHandler = class(TInterfacedObject, ISDContactHandler)
    private
    protected
      procedure ContactHandlerTest(aMessage: string);
    public
  end;


implementation

procedure TSDContactHandler.ContactHandlerTest(aMessage: string);
var
  I: IESInBound;
begin
  if aBusinessClient.Services['ESInBound'].Get(I) then I.ForTest(aMessage);
end;

end.

