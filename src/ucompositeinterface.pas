unit uCompositeInterface;

{$mode ObjFPC}{$H+}

interface

uses
  mORMot;

type

  {Channels -> cross Channel}

  // Version: 2.0.0
  // BIAN defines a customer contact as the overarching structure handling interaction from start to end
  // BIAN将客户联系定义为交互理的全程完整结构
  ISDContactHandler = interface(IInvokable)
    ['{BAD35D53-09C4-4265-A756-CF3C270C6C51}']
    // Activate a SDContactHandler servicing session
    // 激活SDContactHandler服务会话
    //procedure Activate;

    // 测试用
    procedure ContactHandlerTest(aMessage: string);
  end;

const
  ROOT_NAME_COMPOSITE = 'composite';
  PORT_NAME_COMPOSITE = '2022';
  APPLICATION_NAME_COMPOSITE = 'zComposite';

var
  aCompositeClient: TSQLRestClientURI;

implementation

initialization
  // so that we could use directly ISDContactHandler instead of TypeInfo(ISDContactHandler)
  TInterfaceFactory.RegisterInterfaces([TypeInfo(ISDContactHandler)]);
end.

