unit fParty;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, ExtCtrls, StdCtrls, attabs,
  SynCommons,
  uCompositeInterface;

type

  { TFrameParty }

  TFrameParty = class(TFrame)
    btnMessage: TButton;
    cbMessage: TComboBox;
    Panel1: TPanel;
    Splitter1: TSplitter;
    procedure btnMessageClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    //tab: TATTabData;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  FrameParty: TFrameParty;

implementation

{$R *.lfm}

procedure TFrameParty.btnMessageClick(Sender: TObject);
var
  I: ISDContactHandler;
begin
  if aCompositeClient.Services['SDContactHandler'].Get(I) then
    I.ContactHandlerTest(StringToUtf8(cbMessage.Text));
end;

constructor TFrameParty.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFrameParty.Destroy;
begin
  FrameParty:= nil;
  inherited Destroy;
end;

end.

